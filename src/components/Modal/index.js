import { ModalBody, ModalCloseButton, ModalContent, ModalOverlay } from "./Modal.style"

const Modal = ({ show, setValue, children }) => {
  const closeModal = () => {
    setValue(!show)
  }

  return(
    <ModalOverlay show={show}>
      <ModalContent>
        <ModalCloseButton onClick={closeModal}>&times;</ModalCloseButton>
        <ModalBody>
          { children }
        </ModalBody>
      </ModalContent>
    </ModalOverlay>
  )
}

export default Modal