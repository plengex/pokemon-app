import styled from "@emotion/styled"

const StyledContainer = styled.div`
  margin: 0 auto;
  padding: 20px 15px;

  @media only screen and (min-width: 576px) {
    max-width: 540px;
  }

  @media only screen and (min-width: 768px) {
    max-width: 720px;
  }

  @media only screen and (min-width: 992px) {
    max-width: 960px;
  }

  @media only screen and (min-width: 1200px) {
    max-width: 1140px;
  }
`

export default StyledContainer