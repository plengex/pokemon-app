import { Spinner, StyledOverlay } from "./Loading.style"

const Loading = () => {
  return(
    <StyledOverlay>
      <Spinner />
    </StyledOverlay>
  )
}

export default Loading