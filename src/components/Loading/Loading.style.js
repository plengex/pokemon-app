import { keyframes } from "@emotion/react"
import styled from "@emotion/styled"

export const StyledOverlay = styled.div`
  position: fixed;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0, 0, 0, 0.5);
  z-index: 5;
`

export const AnimatedSpinner = keyframes`
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
`

export const Spinner = styled.div`
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #555;
  width: 90px;
  height: 90px;
  -webkit-animation: ${ AnimatedSpinner } 2s linear infinite;
  animation: ${ AnimatedSpinner } 2s linear infinite;
`