import { useHistory } from "react-router-dom"
import { MyPokemonsCount, StyledNavbar } from "./Navbar.style"
import { useCountMyPokemons } from "../../hooks/pokemon"

const Navbar = () => {
  const history = useHistory()
  const myPokemons = useCountMyPokemons()

  const goMyPokemonsList = () => {
    history.push('/my-pokemons')
  }
  
  
  return(
    <StyledNavbar>
      <MyPokemonsCount onClick={goMyPokemonsList}>My Pokemons: { myPokemons }</MyPokemonsCount> 
    </StyledNavbar>
  )
}

export default Navbar