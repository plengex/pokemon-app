import styled from "@emotion/styled"

export const StyledNavbar = styled.div`
  height: 67px;
  display: flex;
  padding: 0.5rem;
  align-items: center;
  box-sizing: border-box;
  background-color: white;
  box-shadow: 0 10px 15px -3px rgb(0 0 0 / 10%), 0 4px 6px -2px rgb(0 0 0 / 5%);

  & h5 {
    margin: 0;
    cursor: pointer;
  }
`

export const MyPokemonsCount = styled.div`
  font-weight: 600;
  cursor: pointer;
`