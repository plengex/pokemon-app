import styled from "@emotion/styled"
import { useContext } from "react"
import { FaTrashAlt } from "react-icons/fa"
import { Link } from "react-router-dom"
import { AppContext } from "../context"
import { countPokemonName } from "../utils"

const Card = styled.div`
  cursor: pointer;
  overflow: hidden;
  position: relative;
  border-radius: 0.5rem;
  background-color: #fff;
  box-shadow: 0 6px 10px rgba(0,0,0,.08), 0 0 6px rgba(0,0,0,.05);
  transition: .3s transform cubic-bezier(.155,1.105,.295,1.12),.3s box-shadow,.3s -webkit-transform cubic-bezier(.155,1.105,.295,1.12);

  &:hover{
    transform: scale(1.05);
    box-shadow: 0 10px 20px rgba(0,0,0,.12), 0 4px 8px rgba(0,0,0,.06);
  }

  & a{
    color: inherit;
    text-decoration: none;
  }
`

const ImageWrapper = styled.div`
  height: 16rem;
  display: flex;
  position: relative;
  align-items: center;
  justify-content: center;
`

const PokemonIdentity = styled.div`
  padding: 1rem;
  text-align: center;
  padding-bottom: 2rem;
`

const PokemonName = styled.h1`
  margin: 0;
  font-weight: 600;
  font-size: 1.75rem;
  margin-bottom: 0.75rem;
  text-transform: capitalize;
`

const OwnPokemonCount = styled.div`
  padding: 0.5rem;
  border-radius: 0.5rem;
  background-color: #e3350d;
  position: absolute;
  z-index: 2;
  color: white;
  top: 20px;
  right: 20px;
`

const ReleaseButton = styled.div`
  padding: 0.5rem;
  border-radius: 0.5rem;
  background-color: #e3350d;
  position: absolute;
  z-index: 2;
  display: block;
  color: white;
  top: 20px;
  right: 20px;

  & svg {
    fill: white;
  }
`

const PokemonCard = (props) => {
  const { ownedPokemons, setOwnedPokemons } = useContext(AppContext)

  const releasePokemon = () => {
    const releasedPokemon = ownedPokemons.filter((pokemon) => pokemon.customName !== props.data.customName)
    setOwnedPokemons(releasedPokemon)
  }

  return (
    <Card>
      {
        props.myPokemons &&
        <ReleaseButton onClick={releasePokemon}>Release Pokemon <FaTrashAlt /></ReleaseButton>
      }
      <Link to={`/pokemons/${props.data.name}`}>
          <ImageWrapper>
            <img src={ props.myPokemons ? props.data.sprites.front_default : props.data.image } alt={props.data.name} />
            {
              props.myPokemons ||
              <OwnPokemonCount>Owned { countPokemonName(ownedPokemons, props.data.name) }</OwnPokemonCount>
            }
          </ImageWrapper>
          <PokemonIdentity>
            <PokemonName>{ props.myPokemons ? props.data.customName : props.data.name }</PokemonName>
          </PokemonIdentity>
      </Link>
    </Card>
  )
}

export default PokemonCard