import { ImageWrapper, PokemonAttr, PokemonDetail, PokemonDetailCard, PokemonName } from "./PokemonDetailsPage.style"
import { useContext, useState } from "react"
import { useHistory, useParams } from "react-router-dom"
import Modal from "../../components/Modal"
import { AppContext } from "../../context"
import { useGetPokemonDetail } from "../../hooks/pokemon"
import { catchPokemon as probability, validatePokemonName } from "../../utils"
import { FaArrowLeft } from "react-icons/fa"

const PokemonDetailsPage = (data) => {
  const { pokemonName } = useParams()
  const history = useHistory()
  const { pokemonsDetail, ownedPokemons, setOwnedPokemons } = useContext(AppContext)
  const [showModalSuccess, setShowModalSuccess] = useState(false)
  const [showModalFail, setShowModalFail] = useState(false)
  const [customName, setCustomName] = useState('')

  useGetPokemonDetail(pokemonName)

  const catchPokemon = () => {
    const catchedProbability = probability()

    if(catchedProbability) {
      setShowModalSuccess(true)
    }
    if(!catchedProbability)
      setShowModalFail(true)
  }

  const saveToMyOwn = () => {
    const validateName = validatePokemonName(ownedPokemons, customName)

    if(validateName)
      setOwnedPokemons([...ownedPokemons, {...pokemonsDetail, customName}])

    if(!validateName)
      alert(`Pokemon dengan nama ${customName} sudah ada, cari nama yang lain yuk`)
      
    setShowModalSuccess(false)
    setCustomName('')
  }

  const goBack = () => {
    history.push('/')
  }

  return(
    <div>
      <h5 onClick={goBack}>
        <FaArrowLeft /> Go Back
      </h5>
      <PokemonDetailCard>
        <ImageWrapper>
          <img src={pokemonsDetail?.sprites?.front_default} alt={pokemonsDetail.name} />
        </ImageWrapper>
        <PokemonDetail>
          <PokemonName>{pokemonsDetail.name}</PokemonName>
          <PokemonAttr>Types</PokemonAttr>
          <ul>
            {
              pokemonsDetail?.types?.slice(0, 3).map(({type}, idx) => {
                return(<li key={idx}>{type.name}</li>)
              })
            }
          </ul>
          <PokemonAttr>Moves</PokemonAttr>
          <ul>
            {
              pokemonsDetail?.moves?.slice(0, 3).map(({move}, idx) => {
                return(<li key={idx}>{move.name}</li>)
              })
            }
          </ul>
          <button onClick={catchPokemon}>Catch Pokemon</button>
        </PokemonDetail>
      </PokemonDetailCard>
      <Modal show={showModalSuccess} setValue={setShowModalSuccess}>
        <h3>Pokemon berhasil ditangkap</h3>
        <p>Ayo beri nama untuk pokemonmu</p>
        <input type="text" value={customName} onChange={(e) => setCustomName(e.target.value)} />
        <button onClick={saveToMyOwn}>Simpan</button>
      </Modal>
      <Modal show={showModalFail} setValue={setShowModalFail}>
        <h3>Pokemon tidak berhasil ditangkap</h3>
        <p>Ayo semangat coba lagi</p>
      </Modal>
    </div>
  )
}

export default PokemonDetailsPage