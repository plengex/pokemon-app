import styled from "@emotion/styled";

export const PokemonDetailCard = styled.div`
  overflow: hidden;
  border-radius: 0.5rem;
  background-color: #fff;
  box-shadow: 0 6px 10px rgba(0,0,0,.08), 0 0 6px rgba(0,0,0,.05);
  display: grid;
  grid-gap: 1.5rem;
  min-height: 21rem;
  padding: 15px;

  @media only screen and (min-width: 576px) {
  }

  @media only screen and (min-width: 768px) {
    grid-template-columns: repeat(3, minmax(0, 1fr));
  }

  @media only screen and (min-width: 992px) {
  }

  @media only screen and (min-width: 1200px) {
  }
`

export const ImageWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  border-bottom: 1px solid #d3d3d3;

  @media only screen and (min-width: 576px) {
  }

  @media only screen and (min-width: 768px) {
    grid-column: span 1 / span 3;
    border: none;
    border-right: 2px solid #d3d3d3;
  }

  @media only screen and (min-width: 992px) {
  }

  @media only screen and (min-width: 1200px) {
  }
`

export const PokemonDetail = styled.div`
  @media only screen and (min-width: 576px) {
  }

  @media only screen and (min-width: 768px) {
    grid-column: span 2 / span 3;
  }

  @media only screen and (min-width: 992px) {
  }

  @media only screen and (min-width: 1200px) {
  }
`

export const PokemonName = styled.div`
  font-size: 1.85rem;
  font-weight: 600;
  line-height: 2.15rem;
  text-transform: capitalize;
  margin-bottom: 1rem;
`

export const PokemonAttr = styled.div`
  font-size: 1.25rem;
  font-weight: 600;
  line-height: 1.55rem;
  margin-bottom: 0.5rem;
`