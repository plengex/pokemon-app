import { useContext } from "react"
import { FaArrowLeft } from "react-icons/fa"
import { useHistory } from "react-router-dom"
import PokemonCard from "../../components/PokemonCard"
import { AppContext } from "../../context"
import { PokemonsList } from "../PokemonsPage/PokemonsPage.style"

const MyPokemonsPage = () => {
  const { ownedPokemons } = useContext(AppContext)
  const history = useHistory()

  const goBack = () => {
    history.push('/')
  }

  return(
    <>
      <h5 onClick={goBack}>
        <FaArrowLeft /> Go Back
      </h5>
      <PokemonsList>
        {
          ownedPokemons.map((item, idx) => {
            return (
              <div key={idx}>
                <PokemonCard myPokemons data={item} />
              </div>
            )
          })
        }
      </PokemonsList>
    </>
  )
}

export default MyPokemonsPage