import styled from "@emotion/styled";

export const ErrorPage = styled.div`
  width: 100%;
  height: calc(100vh - 82px);
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

export const ErrorCode = styled.div`
  font-size: 36px;
  font-weight: 600;
  text-align: center;
`

export const ErrorMessage = styled.div`
  font-size: 24px;
  font-weight: 500;
  text-align: center;
`