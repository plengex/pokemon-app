import { ErrorCode, ErrorMessage, ErrorPage } from "./ErrorPage.style"

const Page404 = () => {
  return(
    <ErrorPage>
      <ErrorCode>404</ErrorCode>
      <ErrorMessage>Halaman Tidak Ditemukan</ErrorMessage>
    </ErrorPage>
  )
}

export default Page404