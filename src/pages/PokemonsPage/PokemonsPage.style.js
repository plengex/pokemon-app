import styled from "@emotion/styled";

export const PokemonsList = styled.div`
  display: grid;
  grid-gap: 1.5rem;

  @media only screen and (min-width: 576px) {
  }

  @media only screen and (min-width: 768px) {
    grid-template-columns: repeat(2, minmax(0, 1fr));
  }

  @media only screen and (min-width: 992px) {
    grid-template-columns: repeat(3, minmax(0, 1fr));
  }

  @media only screen and (min-width: 1200px) {
    grid-template-columns: repeat(4, minmax(0, 1fr));
  }
`