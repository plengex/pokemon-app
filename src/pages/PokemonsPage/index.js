import { useContext } from "react"
import PokemonCard from "../../components/PokemonCard"
import { AppContext } from "../../context"
import { useGetPokemonsList } from "../../hooks/pokemon"
import { PokemonsList } from "./PokemonsPage.style"

const PokemonsPage = () => {
  const { pokemons } = useContext(AppContext)

  useGetPokemonsList()

  return(
    <PokemonsList>
      {
        pokemons.map((item, idx) => {
          return (
            <div key={idx}>
              <PokemonCard data={item} />
            </div>
          )
        })
      }
    </PokemonsList>
  )
}

export default PokemonsPage