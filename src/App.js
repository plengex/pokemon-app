import { useContext } from 'react'
import AppRoutes from './routes'
import { AppContext } from './context'
import Loading from './components/Loading';

const App = () => {
  const { loading } = useContext(AppContext)

  return (
    <>
      { loading && <Loading /> }
      <AppRoutes />
    </>
  );
}

export default App
