import { BrowserRouter, Switch, Route } from "react-router-dom"
import Layout from "../layouts"
import Page404 from "../pages/error/Page404"
import MyPokemonsPage from "../pages/MyPokemonsPage"
import PokemonDetailsPage from "../pages/PokemonDetailsPage/"
import PokemonsPage from "../pages/PokemonsPage/"

const AppRoutes = () => {
  return(
    <BrowserRouter>
      <Layout>
        <Switch>
          <Route path="/pokemons/:pokemonName" component={PokemonDetailsPage} />
          <Route exact path="/" component={PokemonsPage} />
          <Route path="/my-pokemons" component={MyPokemonsPage} />
          <Route exact path="*" component={Page404} />
        </Switch>
      </Layout>
    </BrowserRouter>
  )
}

export default AppRoutes