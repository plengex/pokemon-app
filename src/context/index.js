import { createContext, useState } from "react"
import useLocalStorage from "../hooks/useLocalStorage"

export const AppContext = createContext(null)

export const AppContextProvider = ({ children }) => {
  const [loading, setLoading] = useState(false)
  const [pokemons, setPokemons] = useState([])
  const [pokemonsDetail, setPokemonsDetail] = useState({})
  const [ownedPokemons, setOwnedPokemons] = useLocalStorage('myPokemons', [])

  return(
    <AppContext.Provider value={{
      loading,
      setLoading,
      pokemons,
      setPokemons,
      ownedPokemons,
      setOwnedPokemons,
      pokemonsDetail,
      setPokemonsDetail
    }} >
      { children }
    </AppContext.Provider>
  )
}