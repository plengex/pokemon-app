import { css, Global } from "@emotion/react";

const style = css`
  body {
    margin: 0;
    font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen,
      Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
      sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    background-color: #f2f1f0
  }
`

const GlobalStyle = () => {
  return(
    <Global styles={style} />
  )
}

export default GlobalStyle
