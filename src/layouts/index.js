import Container from "../components/Container"
import Navbar from "../components/Navbar"

const Layout = ({ children }) => {
  return(
    <>
      <Navbar />
      <Container>
        { children }
      </Container>
    </>
  )
}

export default Layout