import { ApolloClient, InMemoryCache, ApolloProvider as Apollo } from "@apollo/client"

const client = new ApolloClient({
  uri: process.env.REACT_APP_GRAPHQL_URL,
  cache: new InMemoryCache()
})

const ApolloProvider = ({ children }) => {
  return(
    <Apollo client={ client }>
      { children }
    </Apollo>
  )
}

export default ApolloProvider