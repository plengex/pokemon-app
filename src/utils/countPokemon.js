const countPokemonName = (ownedPokemons, pokemonName) => {
  return ownedPokemons.filter((pokemon) => pokemon.name === pokemonName).length
}

export default countPokemonName