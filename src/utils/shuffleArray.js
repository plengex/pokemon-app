const shuffleArray = (array) => {
  const buffer = []
  while (buffer.length < array.length) {
    const randomIndex = Math.floor(Math.random() * array.length)
    if (!buffer.includes(array[randomIndex])) buffer.push(array[randomIndex]);
  }
  
  return buffer
}

export default shuffleArray
