const validatePokemonName = (ownedPokemons, pokemonName) => {
  return !!!ownedPokemons.filter((pokemon) => pokemon.customName === pokemonName).length
}

export default validatePokemonName