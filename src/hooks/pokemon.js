import { useQuery } from "@apollo/client"
import { useContext } from "react"
import { AppContext } from "../context"
import { GET_POKEMONS_LIST, GET_POKEMON_DETAIL } from "../graphql"
import { shuffleArray } from "../utils"

export const useGetPokemonsList = () => {
  const { setPokemons, setLoading } = useContext(AppContext)
  const OperationVariables = {
    variables: {
      offset: 1,
      limit: 500
    },
    onCompleted: ({ pokemons }) => { 
      setPokemons(shuffleArray(pokemons.results))
      setLoading(false)
    }
  }

  const { loading } = useQuery(GET_POKEMONS_LIST, OperationVariables)

  if(loading) setLoading(true)
}

export const useGetPokemonDetail = (pokemonName) => {
  const { setPokemonsDetail, setLoading } = useContext(AppContext)
  const OperationVariables = {
    variables: {
      name: pokemonName
    },
    onCompleted: ({ pokemon }) => {
      setPokemonsDetail(pokemon)
      setLoading(false)
    }
  }

  const { loading } = useQuery(GET_POKEMON_DETAIL, OperationVariables)

  if(loading) setLoading(true)
}

export const useCountMyPokemons = () => {
  const { ownedPokemons } = useContext(AppContext)

  return ownedPokemons.length
}